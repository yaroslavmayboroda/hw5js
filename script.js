'use script'


function createNewUser() {
    var newUser = {};
    newUser.firstName = prompt('Введите имя:');
    newUser.lastName = prompt('Введите фамилию:');

    newUser.getLogin = function() {
      var firstNameInitial = this.firstName.charAt(0).toLowerCase();
      return firstNameInitial + this.lastName.toLowerCase();  
    };

    return newUser;
}

var user = createNewUser();
var login = user.getLogin();
console.log(login);
